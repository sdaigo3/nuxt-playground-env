# playground-env

## Run with Docker

```
# build for production
$ npm run build

$ docker compose up -d
```

## Add runtime environment variable

```
# docker-compose.yml
...
  environment:
    - NODE_ENV=environment-1
    - SOME_VAR=foo
```

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start
```
